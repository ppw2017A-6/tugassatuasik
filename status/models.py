# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from user_profile.models import UserProfile

# Create your models here.
class Status(models.Model):
    status = models.TextField(max_length=350)
    user = models.ForeignKey(UserProfile, related_name="statuses", on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
 
    class Meta:
        ordering = ['-date_created']