# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Status
from user_profile.models import UserProfile
from .forms import Status_Form
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
response = {}
def index(request):
    status_list = Status.objects.all()
    html = 'status/status.html'
    paginator = Paginator(status_list, 15)
    page = request.GET.get('page')
    try:
        status = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        status = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        status = paginator.page(paginator.num_pages)
    response['status'] = status
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status=response['status'],user=UserProfile.objects.first())
        status.save()
        messages.info(request, 'Your status has been posted successfully!')
        return HttpResponseRedirect('/')
    else:
        print(form.errors)
        return HttpResponseRedirect('/')