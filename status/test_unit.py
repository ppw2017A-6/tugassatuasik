from django.test import TestCase
from django.test import Client
from friend.models import Friend
from django.core.urlresolvers import reverse
from user_profile.models import UserProfile
from status.models import Status


def create_user():
	return UserProfile.objects.create(name="Nabil", photo_url="https://avatars0.githubusercontent.com/u/22791949?v=4&s=400", gender="male", description="Saya paling jago", email="nabil@tegar.com", birthday="1990-1-1")


class StatusUnitTest(TestCase):

    def test_get_status_success(self):
        user = create_user()
        Status.objects.create(status = "Ini tes", user = user)
        response_get = Client().get(reverse("status:index"))
        self.assertEqual(response_get.status_code, 200)
        html_response = response_get.content.decode('utf8')

        self.assertIn("Ini tes", html_response)
        self.assertIn("Nabil", html_response)

    def test_get_status_invalid_page(self):
        user = create_user()
        Status.objects.create(status = "Ini tes", user = user)
        response_get = Client().get(reverse("status:index")+'?page=100')
        self.assertEqual(response_get.status_code, 200)

    def test_post_status_success(self):
        user = create_user()
        response_post = Client().post(reverse("status:add-status"),{"status": "Norman ganteng"})
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Status.objects.all().count(),1)
        self.assertEqual(Status.objects.first().status, "Norman ganteng")

    def test_post_status_error(self):
        user = create_user()
        response_post = Client().post(reverse("status:add-status"),{"status": ""})
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Status.objects.all().count(),0)
