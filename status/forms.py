from django.forms import ModelForm
from status.models import Status

class Status_Form(ModelForm):
    class Meta:
        model = Status
        fields = ['status']