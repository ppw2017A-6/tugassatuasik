from selenium import webdriver
from django.test import TestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from status.models import Status


class StatusFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StatusFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        status = selenium.find_element_by_id('text-content')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('Woy ini functional test')

        # submitting the form
        submit.send_keys(Keys.RETURN)
