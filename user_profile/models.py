from django.db import models
import datetime

# Create your models here.
class UserProfile(models.Model):
    name = models.CharField(max_length=255)
    photo_url = models.URLField(default="https://avatars0.githubusercontent.com/u/22791949?v=4&s=400")
    birthday = models.DateField(default=datetime.date.today)
    gender = models.CharField(max_length=10)
    description = models.TextField()
    email = models.EmailField()

class Expertise(models.Model):
    user = models.ForeignKey(UserProfile, related_name="expertises", on_delete=models.CASCADE)
    name = models.CharField(max_length=255)