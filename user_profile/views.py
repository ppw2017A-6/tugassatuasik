from django.shortcuts import render
from user_profile.models import UserProfile

# Create your views here.
def index(request):
    profile = UserProfile.objects.first()
    expertises = profile.expertises.all()
    html = 'user_profile/user_profile.html'
    response = {
        'profile':profile,
        'expertises': expertises
    }
    return render(request, html, response)
