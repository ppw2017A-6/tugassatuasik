from django.core.management.base import BaseCommand, CommandError
from user_profile.models import UserProfile, Expertise


class Command(BaseCommand):
    help = 'Initialize User Profile'

    def handle(self, *args, **options):
        user = UserProfile.objects.first()
        if not user:
            self.stdout.write('Initalize user profile first')
        else:
            expertises = Expertise.objects.bulk_create([
                                Expertise(name="Drinking", user=user),
                                Expertise(name="Eating", user=user),
                                Expertise(name="Sleeping", user=user),
                                Expertise(name="Spongebob", user=user),
                            ])
            if expertises:
                self.stdout.write(self.style.SUCCESS('Initialize user expertises success.'))
            else:
                self.stdout.write('Initialize user expertises failed')
