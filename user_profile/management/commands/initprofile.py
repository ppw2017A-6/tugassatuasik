from django.core.management.base import BaseCommand, CommandError
from user_profile.models import UserProfile


class Command(BaseCommand):
    help = 'Initialize User Profile'

    def handle(self, *args, **options):
        profile = UserProfile.objects.create(name="Petrik", photo_url="https://memegenerator.net/img/images/600x600/6861343/patrick-the-troll.jpg", gender="Male", description="I love squidward", email="petrik@ui.ac.id", birthday="1990-1-1")
        if profile:
            self.stdout.write(self.style.SUCCESS('Initialize user profile success.'))
