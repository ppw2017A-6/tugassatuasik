from django.test import TestCase
from django.test import Client
from django.core.urlresolvers import reverse
from django.utils.html import escape
from django.urls import resolve
from user_profile.models import UserProfile, Expertise
from user_profile.views import index


def create_user():
    return UserProfile.objects.create(name="Nabil", photo_url="https://avatars0.githubusercontent.com/u/22791949?v=4&s=400", gender="male", description="Saya paling jago", email="nabil@tegar.com", birthday="1990-1-1")


def create_expertises(user):
    return Expertise.objects.bulk_create([
                Expertise(name="Drinking", user=user),
                Expertise(name="Eating", user=user),
                Expertise(name="Sleeping", user=user),
          ])


class UserProfileUnitTest(TestCase):

    def test_get_profile_success(self):
        user = create_user()
        expertises = create_expertises(user)

        response_get = Client().get(reverse('profile:index'))
        html_response = response_get.content.decode('utf8')
        self.assertIn(user.name, html_response)
        self.assertIn(escape(user.photo_url), html_response)
        self.assertIn(user.gender, html_response)
        self.assertIn(user.description, html_response)
        self.assertIn(user.email, html_response)

        for expertise in expertises:
            self.assertIn(expertise.name, html_response)

    def test_use_index_function(self):
        response = resolve(reverse('profile:index'))
        self.assertEqual(response.func, index)
