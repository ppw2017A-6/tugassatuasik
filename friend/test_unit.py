from django.urls import resolve
from django.test import TestCase
from django.test import Client
from friend.views import index
from friend.models import Friend
from django.core.urlresolvers import reverse
from user_profile.models import UserProfile


def create_user():
    return UserProfile.objects.create(name="Nabil", photo_url="https://avatars0.githubusercontent.com/u/22791949?v=4&s=400", gender="male", description="Saya paling jago", email="nabil@tegar.com", birthday="1990-1-1")


class FriendUnitTest(TestCase):

    def test_post_friend_success(self):
        response_post = Client().post(reverse('friend:add-friend'),
                                      {'name': 'Nama',
                                       'profile_url': 'http://tugassatuasik.herokuapp.com'})
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Friend.objects.all().count(), 1)

        friend = Friend.objects.first()
        self.assertEqual(friend.name, 'Nama')
        self.assertEqual(friend.profile_url, 'http://tugassatuasik.herokuapp.com')

    def test_post_friend_invalid_profile_url(self):
        response_post = Client().post(reverse('friend:add-friend'),
                                      {'name': 'Nama',
                                       'profile_url': 'http://abc'})
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Friend.objects.all().count(), 0)

    def test_post_friend_https_url(self):
        response_post = Client().post(reverse('friend:add-friend'),
                                      {'name': 'Nama',
                                       'profile_url': 'https://tugassatuasik.herokuapp.com'})
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Friend.objects.all().count(), 1)

    def test_post_friend_unresolved_profile_url(self):
        response_post = Client().post(reverse('friend:add-friend'),
                                      {'name': 'Nama',
                                       'profile_url': 'https://abc.abc'})
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Friend.objects.all().count(), 0)

    def test_post_friend_heroku_app_not_found(self):
        response_post = Client().post(reverse('friend:add-friend'),
                                      {'name': 'Nama',
                                       'profile_url': 'https://tugasssatuasikks.herokuapp.com'})
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Friend.objects.all().count(), 0)

    def test_get_friend_success(self):
        user = create_user()
        friend = Friend(name='Nama', profile_url='http://tugassatuasik.herokuapp.com', user=user)
        friend.save()

        response_get = Client().get(reverse('friend:index'))
        html_response = response_get.content.decode('utf8')
        self.assertIn('Nama', html_response)
        self.assertIn('http://tugassatuasik.herokuapp.com', html_response)

    def test_get_friends_with_form_errors(self):
        create_user()
        response_get = Client().get(reverse('friend:index') + '?errors=' +
            '{"name": [{"message": "This field is required.", "code": "required"}]}')
        html_response = response_get.content.decode('utf8')

        self.assertIn('has-error', html_response)

    def test_use_index_function(self):
        response = resolve(reverse('friend:index'))
        self.assertEqual(response.func, index)
