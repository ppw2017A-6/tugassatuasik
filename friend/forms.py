import http.client
from django.forms import ModelForm, ValidationError, CharField
from friend.models import Friend


def is_profile_url_valid(profile_url):
    if 'https://' in profile_url:
        profile_url = profile_url[8:]
    else:
        profile_url = profile_url[7:]

    try:
        req = http.client.HTTPConnection(profile_url)
        req.request("GET", "/")
        response = req.getresponse()
        return response.status == 200 or response.status == 301 or response.status == 302

    except Exception:
        raise ValidationError("Given page does not exist")


class FriendForm(ModelForm):

    photo_url = CharField(required=False)

    class Meta:
        model = Friend
        fields = ['name', 'profile_url', 'photo_url']

    def clean_profile_url(self):
        profile_url = self.cleaned_data['profile_url']
        if is_profile_url_valid(profile_url):
            return profile_url
        raise ValidationError("Given page does not exist")

    def clean_photo_url(self):
        return self.cleaned_data['photo_url'] or "https://cdn.movemeback.com/movemeback/images/noprofile.png"
