import json
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from friend.forms import FriendForm
from user_profile.models import UserProfile


def index(request):

    friends = UserProfile.objects.first().friends.all()
    html = 'friend/friend.html'
    friend_form = FriendForm()

    response = {
        'friends': friends,
        'friend_form': friend_form,
    }

    error_json = request.GET.get('errors', None)

    if error_json is not None:
        errors = json.loads(error_json)
        response['errors'] = errors

    return render(request, html, response)


def add_friend(request):
    if request.method == 'POST':
        friend_form = FriendForm(request.POST)
        if friend_form.is_valid():
            friend = friend_form.save(commit=False)
            friend.user = UserProfile.objects.first()
            friend.save()

        else:
            return HttpResponseRedirect(reverse('friend:index') + '?errors=' +
                                        friend_form.errors.as_json())

    return HttpResponseRedirect(reverse('friend:index'))
