from selenium import webdriver
from django.test import TestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from user_profile.models import UserProfile


class FriendFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FriendFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FriendFunctionalTest, self).tearDown()

    def test_input_friend(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/friends/')
        # find the form element
        name = selenium.find_element_by_id('id_name')
        profile_url = selenium.find_element_by_id('id_profile_url')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        name.send_keys('Tugas 1')
        profile_url.send_keys('http://tugassatuasik.herokuapp.com')

        # submitting the form
        submit.send_keys(Keys.RETURN)
