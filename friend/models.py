from django.db import models
from user_profile.models import UserProfile


# Create your models here.
class Friend(models.Model):
    name = models.CharField(max_length=255)
    photo_url = models.URLField(default="https://cdn.movemeback.com/movemeback/images/noprofile.png")
    profile_url = models.URLField()
    date_created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(UserProfile, related_name="friends",
                             on_delete=models.CASCADE, null=True)

    class Meta:
        ordering = ['-date_created']
