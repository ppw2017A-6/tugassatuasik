from django.test import TestCase
from django.test import Client
from django.core.urlresolvers import reverse
from django.utils.html import escape
from django.urls import resolve
from statistic.views import index
from user_profile.models import UserProfile
from status.models import Status
from friend.models import Friend


def create_user():
    return UserProfile.objects.create(name="Nabil", photo_url="https://avatars0.githubusercontent.com/", gender="male", description="Saya paling jago", email="nabil@tegar.com", birthday="2000-3-3")


class StatisticUnitTest(TestCase):

    def test_get_statistic_success(self):
        user = create_user()
        Friend.objects.create(user=user, name="Nama", profile_url="http://facebook.com")
        Status.objects.create(user=user, status="Statusa")
        Status.objects.create(user=user, status="Statusb")
        Status.objects.create(user=user, status="Statusc")
        Status.objects.create(user=user, status="Statusd")

        response_get = Client().get(reverse('stats:index'))

        html_response = response_get.content.decode('utf8')
        self.assertIn(user.name, html_response)
        self.assertIn(escape(user.photo_url), html_response)
        self.assertIn(str(Friend.objects.all().count()), html_response)
        self.assertIn(str(Status.objects.all().count()), html_response)
        self.assertIn(Status.objects.first().status, html_response)

    def test_use_index_function(self):
        response = resolve(reverse('stats:index'))
        self.assertEqual(response.func, index)
