from django.shortcuts import render
from django.http import HttpResponseRedirect
from status.models import Status
from friend.models import Friend
from user_profile.models import UserProfile

response = {}
def index(request):
    response['author'] = UserProfile.objects.first()

    feed = len(Status.objects.all())
    response['feed'] = feed

    friends = len(Friend.objects.all())
    response['friends'] = friends

    latest_post = Status.objects.first()
    response['latest_post'] = latest_post

    html = 'statistic/statistic.html'
    return render(request, html, response)
